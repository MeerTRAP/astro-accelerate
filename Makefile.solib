################################################################################
# Makefile for astro-accelerate
#
# Description: Custom Makefile that can be used instead of CMakeLists.txt
#
################################################################################
CUDA	:= $(CUDA_INSTALL_PATH)
INC	:= -I$(CUDA)include -I$(CUDA)samples/common/inc/ -Idetail  
LIB	:= -L$(CUDA)/lib64
BUILD_DIR	:=./obj/
ASTROLIB_DIR	:= lib/
SRC_DIR		:= src/
CC = g++
OBJ_DIR = obj/

CXXFLAGS	:= -O3
LDFLAGS		= `root-config --libs`
COMPILEJOBS	= astro-accelerate
HEADERS		= include/*.hpp include/*.cuh
INCLUDE		= -Iinclude -Iinclude/cuda

#CU_FILES = ${SRD_DIR}/device_SPS_inplace_kernel.cu ${SRC_DIR}/aa_device_load_data.cu
#CPP_FILES = ${SRC_DIR}/aa_ddtr_plan.cpp ${SRC_DIR}/aa_ddtr_strategy.cpp ${SRC_DIR}/aa_ddtr.cpp ${SRC_DIR}/aa_sps_plan.cpp ${SRC_DIR}/aa_sps_strategy.cpp
#OBJ_FILES = ${OBJ_DIR}/device_SPS_inplace_kernel.o ${OBJ_DIR}/aa_device_load_data.o ${OBJ_DIR}/device_zero_dm_kernel.o ${OBJ_DIR}/aa_zero_dm.o ${OBJ_DIR}/aa_corner_turn.o ${OBJ_DIR}/aa_ddtr_plan.o ${OBJ_DIR}/aa_ddtr_strategy.o ${OBJ_DIR}/aa_ddtr.o\
#${OBJ_DIR}/aa_sps_plan.o ${OBJ_DIR}/aa_sps_strategy.o

CPP_FILES := $(wildcard src/*.cpp)
CU_FILES := ${wildcard src/*.cu}
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o))) $(addprefix obj/,$(notdir $(CU_FILES:.cu=.o)))

# CUDA code generation flags
GENCODE_SM35	:= -gencode arch=compute_35,code=sm_35 # Kepler
GENCODE_SM37	:= -gencode arch=compute_37,code=sm_37 # Kepler
GENCODE_SM50	:= -gencode arch=compute_50,code=sm_50 # Maxwell
GENCODE_SM52	:= -gencode arch=compute_52,code=sm_52 # Maxwell
GENCODE_SM53	:= -gencode arch=compute_53,code=sm_53 # Maxwell
GENCODE_SM60	:= -gencode arch=compute_60,code=sm_60 # Pascal
GENCODE_SM61	:= -gencode arch=compute_61,code=sm_61 # Pascal
GENCODE_SM70	:= -gencode arch=compute_70,code=sm_70 # Volta
GENCODE_FLAGS   := $(GENCODE_SM53)

ifeq ($(cache),off)
        NVCCFLAGS := $(INC) ${INCLUDE} -g -lineinfo -Xcompiler -O3 -lm --use_fast_math\
        --ptxas-options=-v -std=c++11 -Xptxas -dlcm=cg $(GENCODE_FLAGS) -Xcompiler "-fPIC"
else
        NVCCFLAGS := $(INC) ${INCLUDE} -g -lineinfo -Xcompiler -O3 -lm --use_fast_math\
        --ptxas-options=-v -std=c++11 -lcuda -lcudart  -lcurand -lcufft -lcudadevrt -Xptxas -dlcm=cg $(GENCODE_FLAGS) -Xcompiler "-fPIC"
endif

LIBJOBS := libastroaccelerate.so

all:	MAKE_OBJ_FOLDER ${LIBJOBS}

$(BUILD_DIR)%.o :	${SRC_DIR}%.cu
			@echo Compiling $@ ...
			nvcc $(NVCCFLAGS) -c -o $@ $<

$(BUILD_DIR)%.o :	${SRC_DIR}%.cpp
			@echo Compiling $@ ...
			nvcc $(NVCCFLAGS) -c -o $@ $<

MAKE_OBJ_FOLDER :
			mkdir -p obj/

libastroaccelerate.so: ${OBJ_FILES}
			@echo Making shared library libastroaccelerate.so
			$(CC) --shared -o ${ASTROLIB_DIR}/libastroaccelerate.so ${OBJ_FILES}
clean:
	rm -f ${OBJ_DIR}*.o ${ASTROLIB_DIR}/libastroaccelerate.so
