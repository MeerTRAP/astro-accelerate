#ifndef ASTRO_ACCELERATE_MEDIAN_HPP
#define ASTRO_ACCELERATE_MEDIAN_HPP

namespace astroaccelerate {

float median(float arr[], int n);

} //namespace astroaccelerate
  
#endif
