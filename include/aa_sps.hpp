#ifndef ASTRO_ACCELERATE_SPS_HPP
#define ASTRO_ACCELERATE_SPS_HPP

#include <utility>
#include <vector>

#include "aa_ddtr_strategy.hpp"
#include "aa_sps_strategy.hpp"

namespace astroaccelerate {

    struct aa_cand {
        float time;
        float dm;
        float snr;
        float width;
    };

    class aa_sps {
    private:
    
	    // NOTE: Moved from SPS Parameters - I want this to be the property of the whole search
	    bool _verbose;

        aa_sps_strategy _sps_strategy;

        size_t _max_candidates;
        size_t _previous_candidates;
	    size_t _number_candidates;
	    float *_candidate_list;
    
    public:

        aa_sps(void) = delete;

        aa_sps(aa_sps_strategy const& sps_strategy);

        ~aa_sps(void) = default;

        int run_sps(float *d_input, float *candidate_list, std::pair<int, int> current_chunk);

        void convert_candidates(float *candidate_list, std::vector<float> &output_candidates, std::pair<int, int> const& current_chunk, aa_ddtr_strategy const &ddtr_strategy, size_t processed_samples, float sampling_time);

        void sort_candidates(std::vector<float> &output_candidates);

        void update_details(std::pair<int, int> current_chunk);
    };

} // astroaccelerate

#endif
