#ifndef ASTRO_ACCELERATE_SPS_PLAN_HPP
#define ASTRO_ACCELERATE_SPS_PLAN_HPP

#include <stdio.h>
#include <vector>
#include "aa_msd_parameters.hpp"
#include "DedispersionStrategy.h"

namespace astroaccelerate {

class aa_sps_plan {
public:
    aa_sps_plan(void);

    // NOTE: Passed from Cheetah pipeline - mix of DDTR strategy and SPS plan parameters
    aa_sps_plan(DedispersionStrategy const& dedispersion_strategy);

    aa_sps_plan(int candidate_algorithm, float max_boxcar_width_in_sec, float sigma_cutoff, float sigma_constant);

    ~aa_sps_plan() = default;
 /*   
    float algorithm(void) const;

    float max_boxcar_width_in_sec(void);

    float threshold(void) const;

    float sigma_constant(void) const;

    std::vector<int> decimate_limits(void) const;
*/
    float algorithm(void) const {
        return _candidate_algorithm;
    }

    float max_boxcar_width_in_sec(void) const {
        return _max_boxcar_width_in_sec;
    }

    float threshold(void) const {
        return _sigma_cutoff;
    }

    float sigma_constant(void) const {
        return _sigma_cutoff;
    }

    std::vector<int> decimate_limits(void) const {
        return _decimate_bc_limits;
    }

private:
	    // NOTE: These will be actually used
        int _candidate_algorithm;
	    
        float _max_boxcar_width_in_sec;
	    float _sigma_cutoff;
        float _sigma_constant;        
        
        std::vector<int> _decimate_bc_limits;
        
	    size_t _max_candidate_array_size_in_bytes;

};

} // namespace astroaccelerate

#endif /* ASTRO_ACCELERATE_SPS_PLAN_HPP */
