#ifndef ASTRO_ACCELERATE_DEVICE_THRESHOLD_KERNEL_HPP
#define ASTRO_ACCELERATE_DEVICE_THRESHOLD_KERNEL_HPP

#include <cuda.h>
#include <cuda_runtime.h>
#include "params.hpp"

namespace astroaccelerate {

void call_kernel_THR_GPU_WARP(const dim3 &grid_size, const dim3 &block_size,
			      float const *const d_input, ushort *const d_input_taps, unsigned int *const d_output_list_DM,
			      unsigned int *const d_output_list_TS, float *const d_output_list_SNR,
			      unsigned int *const d_output_list_BW, int *const gmem_pos,
			      const float &threshold, const int &nTimesamples, const int &offset, const int &shift, const int &max_list_size,
			      const int &DIT_value);

void call_kernel_GPU_Threshold_for_periodicity_kernel_old(const dim3 &grid_size, const dim3 &block_size,
							  float const *const d_input, ushort *const d_input_harms,
							  float *const d_output_list, int *const gmem_pos, float *const d_MSD,
							  const float &threshold, const int &primary_size,
							  const int &secondary_size, const int &DM_shift,
							  const int &max_list_size, const int &DIT_value);

void call_kernel_GPU_Threshold_for_periodicity_kernel(const dim3 &grid_size, const dim3 &block_size,
						      float const *const d_input, ushort *const d_input_harms,
						      float *const d_output_list, int *const gmem_pos, float const *const d_MSD,
						      const float &threshold, const int &primary_size, const int &secondary_size,
						      const int &DM_shift, const int &max_list_size, const int &DIT_value);

__global__ void THR_GPU_WARP(float const* __restrict__ d_input, ushort *d_input_taps, float *d_output_list, 
								int *gmem_pos, float threshold, int nTimesamples, int offset, int shift,
								int max_list_size, int DIT_value, float sampling_time, 
								float inBin, float start_time);

} //namespace astroaccelerate
#endif
