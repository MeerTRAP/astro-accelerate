//
//  aa_permitted_pipelines_0.hpp
//  aapipeline
//
//  Created by Cees Carels on Friday 02/11/2018.
//  Copyright © 2018 Astro-Accelerate. All rights reserved.
//

#ifndef ASTRO_ACCELERATE_PERMITTED_PIPELINES_0_HPP
#define ASTRO_ACCELERATE_PERMITTED_PIPELINES_0_HPP

#include <stdio.h>

namespace astroaccelerate {

void run_pipeline_0();

}

#endif /* ASTRO_ACCELERATE_PERMITTED_PIPELINES_0_HPP */
