//
//  aa_analysis_strategy.hpp
//  aapipeline
//
//  Created by Cees Carels on Tuesday 23/10/2018.
//  Copyright © 2018 Astro-Accelerate. All rights reserved.
//

#ifndef ASTRO_ACCELERATE_SPS_STRATEGY_HPP
#define ASTRO_ACCELERATE_SPS_STRATEGY_HPP

#include <stdio.h>
#include <vector>

#include "aa_ddtr_strategy.hpp"
#include "aa_sps_plan.hpp"
#include "aa_filterbank_metadata.hpp"
#include "aa_msd_parameters.hpp"

namespace astroaccelerate {

    struct IterationDetails {
            int decimated_timesamples;
            int dtm;
            int iteration;
            int number_blocks;
            int number_boxcars;
            int output_shift;
            int shift;
            int start_taps;
            int total_unprocessed;
            int unprocessed_samples;
    };

    class aa_sps_strategy {
    public:

        aa_sps_strategy(void) = delete;

        aa_sps_strategy(aa_ddtr_strategy const& ddtr_strategy, aa_sps_plan const& sps_plan, aa_filterbank_metadata const& filterbank_metadata);

        aa_sps_strategy(aa_sps_strategy const& sps_strategy);

        ~aa_sps_strategy() = default;

        /**
         * @brief Returns boxcar width for a given widths vector and element within the vector.
         * 
         * The function checks for the out-of-range access - returns the last element if element beyond the size of the vector is requested.
         * 
         * @param element index of the element to return 
         * @param wodths vector of boxcar widths to read values from
         * @return int the requested boxcar width
         */
        int get_boxcar_width(int element, std::vector<int> &widths) const;
        /**
         * @brief Calculates the number of averaging iterations
         * 
         * @param max_width_performed 
         * @param max_boxcar_width boxcar width requested by the used
         * @return pair containing the number of SPS averaging interations and maximum boxcar width searched with
         */
        std::pair<int, size_t> calculate_max_iterations(int max_boxcar_width);

        /**
         * @brief Creates a list of boxcar widths
         * 
         */
        void generate_boxcars_widths(void);

        void generate_processing_details(int current_range, int current_time);

        void print_sps_strategy(void) const;

        int *inbin(void) const {
            return _in_bin;
        }

        float sampling_time(void) const {
            return _filterbank_metadata.tsamp();
        }

        size_t max_candidates(void) const {
            return _max_candidates;
        }

        size_t current_time_samples(void) const {
            return _current_time_samples;
        }

        int *number_dms(void) const {
            return _ndms;
        }

        std::vector<int> get_boxcar_widths(void) const {
            return _boxcar_widths;
        }

        int get_max_iterations(void) const {
            return _iterations;
        }

        int get_algorithm(void) const {
            return _candidate_algorithm;
        }

        float get_threshold(void) const {
            return _sigma_cutoff;
        }

        int get_max_performed_boxcar_width(void) const {
            return _max_boxcar_width_performed;
        }

        aa_msd_parameters get_msd_params(void) const {
            return _msdparams;
        }

        IterationDetails get_details(int iteration) {
            return _details.at(iteration);
        }

    private:

            // NOTE: Replaced with total_unprocessed
            //int total_ut;
            size_t _max_candidates;
            size_t _max_boxcar_width_desired;

            int *_ndms;

            int _nranges;
            int _ntchunks;

            std::vector<int> _boxcar_widths;
            // NOTE: Every iteration will have different strategy
            std::vector<IterationDetails> _details;

            int _candidate_algorithm;
            int _current_time_samples;
            int *_in_bin;
            int **_t_processed;
            int _iterations;
            size_t _max_boxcar_width_performed;

	        float _sigma_cutoff;
            float _sigma_constant;        
        
            std::vector<int> _decimate_bc_limits;
            // TODO: What to do with these bad boys?
            aa_msd_parameters _msdparams;
            
            aa_filterbank_metadata _filterbank_metadata;
    };

} // namespace astroaccelerate

#endif /* ASTRO_ACCELERATE_SPS_STRATEGY_HPP */
