//
//  aa_ddtr_strategy.hpp
//  aapipeline
//
//  Created by Cees Carels on Tuesday 23/10/2018.
//  Copyright © 2018 Astro-Accelerate. All rights reserved.
//

#ifndef ASTRO_ACCELERATE_DDTR_STRATEGY_HPP
#define ASTRO_ACCELERATE_DDTR_STRATEGY_HPP

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

#include "params.hpp"
#include "aa_ddtr_plan.hpp"
#include "aa_filterbank_metadata.hpp"
#include "DedispersionStrategy.h"


namespace astroaccelerate {

class aa_ddtr_strategy {
private:
    bool is_setup;  //Has setup been called already?

    bool _enable_zero_dm;
    bool _enable_zero_dm_outliers;
    bool _enable_rfi;

    //aa_filterbank_metadata m_metadata;
    int _maxshift;
    float* _dm_low;
    float* _dm_high;
    float* _dm_step;
    float* _dm_shifts;
    int* _ndms;
    int _max_ndms;
    int _total_ndms;
    float _max_dm;
    int** _t_processed;
    int _n_ranges;
    int _num_tchunks;
    int *_in_bin;
    int *_out_bin;
    int _nchans;
    int _max_samps;

public:
    aa_ddtr_strategy() = delete;
    // NOTE: This version accepts ddtr_plan and has to compute everything
    aa_ddtr_strategy(aa_ddtr_plan const& ddtr_plan);

    // NOTE: Dedispersion strategy has all of the required stuff precomputed
    aa_ddtr_strategy(DedispersionStrategy const& dedispersion_strategy);

    // NOTE: We need copy constructor to avoid shallow copying
    aa_ddtr_strategy(aa_ddtr_strategy const& ddtr_strategy);

    // aa_ddtr_strategy(const aa_ddtr_plan &plan, const aa_filterbank_metadata &metadata, const size_t &free_memory, const bool &enable_analysis);
    ~aa_ddtr_strategy();
    
    void print_ddtr_strategy(void) const;

    bool enable_zero_dm(void) const {
        return _enable_zero_dm;
    }

    bool enable_zero_dm_outliers(void) const {
        return _enable_zero_dm_outliers;
    }

    bool enable_rfi(void) const {
        return _enable_rfi;
    }

    int* inbin(void) const {
        return _in_bin;
    }

    int* outbin(void) const {
        return _out_bin;
    }

    int maxshift(void) const {
        return _maxshift;
    }
    
    int nchans(void) const {
        return _nchans;
    }

    int** t_processed(void) const {
        return _t_processed;
    }
    
    float* dmshifts(void) const {
        return _dm_shifts;
    }
    
    float* dmlow(void) const {
        return _dm_low;
    }

    float* dmhigh(void) const {
        return _dm_high;
    }

    float* dmstep(void) const {
        return _dm_step;
    }

    int* ndms(void) const {
        return _ndms;
    }
    
    int max_samps(void) const {
        return _max_samps;
    }

    int num_tchunks(void) const {
        return _num_tchunks;
    }

    int ranges() const {
        return _n_ranges;
    }
    
    int max_ndms() const {
        return _max_ndms;
    }
    
    int total_ndms() const {
        return _total_ndms;
    }
    
    float maxdm(void) const {
        return _max_dm;
    }


private:
    


};

} //namespace astroaccelerate

#endif /* ASTRO_ACCELERATE_DDTR_STRATEGY */
