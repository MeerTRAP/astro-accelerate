#ifndef ASTRO_ACCELERATE_MSD_PARAMETERS_HPP
#define ASTRO_ACCELERATE_MSD_PARAMETERS_HPP

#include <iostream>

class aa_msd_parameters {
public:
	// Constants
	float OR_sigma_multiplier;
	// Switches
	int enable_outlier_rejection;
	
	aa_msd_parameters(){
		enable_outlier_rejection = 1;
		OR_sigma_multiplier = 3.0f;
	}
	
	void set(float t_OR_sigma_multiplier, int t_enable_outlier_rejection){
		OR_sigma_multiplier      = t_OR_sigma_multiplier;
		enable_outlier_rejection = t_enable_outlier_rejection;
	}
	
	void debug(){
		std::cout << "MSD parameters:" << std::endl
					<< "enable_outlier_rejection = " << enable_outlier_rejection << std::endl
					<< "OR_sigma_multiplier = " <<  OR_sigma_multiplier << std::endl;
	}
};

#endif