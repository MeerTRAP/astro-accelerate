//
//  aa_ddtr_plan.hpp
//  aapipeline
//
//  Created by Cees Carels on Tuesday 23/10/2018.
//  Copyright © 2018 Astro-Accelerate. All rights reserved.
//

#ifndef ASTRO_ACCELERATE_DDTR_PLAN_HPP
#define ASTRO_ACCELERATE_DDTR_PLAN_HPP

#include <stdio.h>
#include <vector>

#include "DedispersionStrategy.h"

namespace astroaccelerate {

    /**
     * @brief This class stores basic information passed by the user on the dedispersion setup.
     * 
     */
    class aa_ddtr_plan {

    private:  
        int _n_ranges;

    public: // TODO make private
        float *user_dm_low;
        float *user_dm_high;
        float *user_dm_step;
        int *inBin;
        int *outBin;
    
    public:

        aa_ddtr_plan(void) = delete;
        aa_ddtr_plan(DedispersionStrategy const& dedispersion_strategy);
        ~aa_ddtr_plan(void) = default;

        int ranges(void) const;
    };

} // namespace astroaccelerate

#endif /* ASTRO_ACCELERATE_DDTR_PLAN_HPP */
