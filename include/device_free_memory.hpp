#ifndef ASTRO_ACCELERATE_DEVICE_FREE_MEMORY_HPP
#define ASTRO_ACCELERATE_DEVICE_FREE_MEMORY_HPP

namespace astroaccelerate {

extern void free_device_memory(float *device_pointer);

} //namespace astroaccelerate
  
#endif

