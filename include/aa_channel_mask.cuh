#ifndef ASTRO_ACCELERATE_CHANNEL_MASK_HPP
#define ASTRO_ACCELERATE_CHANNEL_MASK_HPP

namespace astroaccelerate {

    __global__ void thresholdMask(unsigned short * __restrict__ data, int nchans, int nsamps, unsigned short threshold);

    void channel_mask(unsigned short *const d_input, const int nchans, const int nsamp);

}

#endif