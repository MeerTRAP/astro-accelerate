#ifndef ASTROACCELERATE_ASTROACCELERATE_H
#define ASTROACCELERATE_ASTROACCELERATE_H

// TODO: Do we actually need all these headers?

#include "DedispersionStrategy.h"
#include "DmTime.hpp"
//
#include "headers_mains.hpp"

#include "aa_bin_gpu.hpp"
#include "aa_device_info.hpp"
//#include "device_dedisperse.h"
//#include "device_dedispersion_kernel.h"
//#include "device_zero_dm.h"
//#include "device_zero_dm_outliers.h"
//#include "device_rfi.h"


//#include "device_SPS_inplace_kernel.h" //Added by KA
//#include "device_SPS_inplace.h" //Added by KA
//#include "device_MSD_BLN_grid.h" //Added by KA
//#include "device_MSD_BLN_pw.h" //Added by KA
//#include "headers/device_MSD_BLN_pw_dp.h" //Added by KA
//#include "device_MSD_grid.h" //Added by KA
//#include "device_MSD_plane.h" //Added by KA
//#include "device_MSD_limited.h" //Added by KA
//#include "device_SNR_limited.h" //Added by KA
//#include "device_SPS_long.h" //Added by KA
//#include "device_threshold.h" //Added by KA
//#include "device_single_FIR.h" //Added by KA
//#include "device_analysis.h" //Added by KA

//#include "device_peak_find.h" //Added by KA

//#include "device_load_data.h"
//#include "device_corner_turn.h"
//#include "device_save_data.h"
//#include "host_acceleration.h"
//#include "host_allocate_memory.h"
//#include "host_analysis.h"
//#include "host_periods.h"
//#include "host_debug.h"
//#include "host_get_file_data.h"
//#include "host_get_recorded_data.h"
//#include "host_get_user_input.h"
//#include "host_rfi.h"
//#include "host_write_file.h"

// fdas
//#include "device_acceleration_fdas.h"

//#include "host_main_function.h"

#include "params.hpp"

#include "gpu_timer.hpp"
#include <vector>

#include "aa_filterbank_metadata.hpp"
#include "aa_ddtr_plan.hpp"
#include "aa_ddtr_strategy.hpp"
#include "aa_sps_plan.hpp"
#include "aa_sps_strategy.hpp"


namespace astroaccelerate {

/*fch1
 *fch1
 *fch1
 *fch1
 *fch1Sps Algorithm
 *fch1
 *fch1
 *fch1
 *fch1
 * 		// dedispersion strategy
 * 		DedispersionStrategy dedispersion_strategy( list of parameters ...);
 * 		// dedispersed data
 *		DmTime<float> output_buffer(dedispersion_strategy);
 *		// output of sps - assume it's a quarter of the output size
 *		std::vector<float> output_sps;
 *		size_t max_peak_size = (size_t) (dedispersion_strategy.get_nsamp() * dedispersion_strategy.get_nchans()/4);
 *		output_sps.resize(max_peak_size*4);
 *		astroaccelerate::AstroAccelerate<TestParams> astroaccelerate(dedispersion_strategy);
 *		astroaccelerate.run_dedispersion_sps(device_id
 *											,dedispersion_strategy
 *											,input_buffer
 *											,output_buffer
 *											,output_sps
 *											);
 * @endcode
 * @endexample
 */

template<typename AstroAccelerateParameterType>
class AstroAccelerate
{
	public:
        AstroAccelerate(void) = delete;
        AstroAccelerate(DedispersionStrategy const& dedispersion_strategy);
        ~AstroAccelerate() = default;

        /**
         * @brief perform dedispersion , sps search, fdas
         * todo: data type for sps and fdas output instead of a vector.
         * sps: dm, timesample, snr, boxcar
         * fdas, f', f, SNR, DM
         * @param device id: the device on which the code should run
         * @param dedispersion_strategy: metadata
         * @param input_buffer: buffer of unsigned short, size number of samples times number of frequency channels
         * @param output_buffer: the dedispersion plan
         * @param output_sps: a vector containing the list of candidates
         * 		  1 candidate is represented by 4 floats: dm, time sample, signal to noise ratio, boxcar filter used
         *
         */
        void run_dedispersion_sps(unsigned device_id
                                  ,unsigned char *input_buffer
                                  ,DmTime<float> &output_buffer
                                  ,std::vector<float> &output_sps
                                  );
        void run_dedispersion_sps(unsigned device_id
        			  ,unsigned short *input_buffer
        			  ,DmTime<float> &output_buffer
        			  ,std::vector<float> &output_sps
        			  );

        long int get_processed_time();

    private:

        aa_filterbank_metadata _filterbank_metadata;

        // NOTE: !!! These have to be kept in this order - plan before strategy!!!
        aa_ddtr_plan _ddtr_plan;
        aa_ddtr_strategy _ddtr_strategy;

        aa_sps_plan _sps_plan;
        aa_sps_strategy _sps_strategy;

        bool _verbose;

        // TODO: We don't really need to store all these parameters separately - they can be stored in DDTR ans SPS plans and strategies
        /**
         * @brief This function allocates memory for the the gpu arrays based on the dedispersion strategy
         */
        void allocate_memory_gpu();

        /**
         * @brief The number of chunks the data are divided in
         */
        int _num_tchunks;
        /**
         * @brief The number of dm range
         */
        int _range;
        /**
         * @brief The number of frequency channels
         */
        int _nchans;
        /**
         * @brief This value is used to make sure that dms from dm_low to dm_high are used
         */
        int _maxshift;
        /**
         * @brief Time sample value
         */
        float _tsamp;
        /**
         * @brief The maximum number of dm
         */
        int _max_ndms;
        /**
         * @brief The threshold for single pulse detection, multiple of standard deviation
         */
        float _sigma_cutoff;
        /**
         * @brief An array containing the number of dms for each range
         */
        int* _ndms;
        /**
         * @brief An array containing a constant associated with each channel to perform dedispersion algorithm
         */
        float* _dmshifts;
        /**
         * @brief The number of time samples required to search for a dm in each dm range
         */
        int** _t_processed;
        /**
         * @brief An array containing the lowest bound of each dm range
         */
        float* _dm_low;
        /**
         * @brief An array containing the highest bound of each dm range
         */
        float* _dm_high;
        /**
         * @brief An array containing the step size bound of each dm range
         */
        float* _dm_step;
        /**
         * @brief ---
         */
        int* _in_bin;
        /**
         * @brief ---
         */
        int* _out_bin;
        

        long int _inc;

        int   _nsamp;
        float _sigma_constant;
        float _max_boxcar_width_in_sec;
        int _total_ndms;
        float _tsamp_original;

        int   _nboots;
        int   _ntrial_bins;
        float _navdms;
        float _narrow;
        float _wide;
        int   _nsearch;
        float _aggression; 
        int _nbits;

        /**
         * @brief Size of the gpu input buffer
         */
        //
        size_t _gpu_input_size;
        /**
         * @brief Gpu input buffer
         */
        unsigned short *_d_input;
        /**
         * @brief Size of the gpu output buffer
         */
        size_t _gpu_output_size;
        /**
         * @brief Gpu output buffer
         */
        float *_d_output;

        // temporary boolean, will disappear eventually
        /**
         * @brief 1 if multiple input files, 0 either
         */
        int _multi_file;
        /**
         * @brief 1 to turn debug on, 0 otherwise
         */
        int _enable_debug;
        /**
         * @brief 1 to turn single pulse search on, 0 otherwise
         */
        int _enable_analysis;
        /**
         * @brief 1 to turn search for periodic objects on, 0 otherwise
         */
        int _enable_periodicity;
        /**
         * @brief 1 to turn fourier domain acceleration search on, 0 otherwise
         */
        int _enable_acceleration;
        /**
         * @brief 1 to turn output dedispspersed data on, 0 otherwise
         */
        int _output_dmt;
        /**
         * @brief 1 to turn analysis with dm=0 on, 0 otherwise
         */
        int _enable_zero_dm;
        /**
         * @brief 1 to turn analysis with dm=0 + outliers on, 0 otherwise
         */
        int _enable_zero_dm_with_outliers;
        /**
         * @brief 1 to turn rfi mitigation on, 0 otherwise
         */
        int _enable_rfi;
        /**
         * @brief 1 to run search with custom FFT, 0 for basic search
         */
        int _enable_fdas_custom_fft;
        /**
         * @brief 1 to perform interbinning on the complex output, 0 otherwise
         */
        int _enable_fdas_inbin;
        /**
         * @brief 1 to perform normalisation, 0 otherwise
         */
        int _enable_fdas_norm;
        /**
         * @brief 1 to use old thresholding code, 0 to use new peak finding code
         */
        int _candidate_algorithm;
        /**
         * @brief 1 to [...], 0 to [...] // TODO: complete it
         */
        int _enable_sps_baselinenoise;
};

} // namespace astroaccelerate

#include "AstroAccelerate.cpp"

#endif // ASTROACCELERATE_ASTROACCELERATE_H
