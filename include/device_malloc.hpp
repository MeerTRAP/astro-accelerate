#ifndef ASTRO_ACCELERATE_DEVICE_MALLOC_HPP
#define ASTRO_ACCELERATE_DEVICE_MALLOC_HPP

namespace astroaccelerate {

//It looks as though this function prototype is declared but not implemented or used anywhere.
extern float *malloc_gpu(size_t size, int zero_mem);

} //namespace astroaccelerate
  
#endif

