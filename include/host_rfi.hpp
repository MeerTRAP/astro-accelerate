#ifndef ASTRO_ACCELERATE_HOST_RFI_HPP
#define ASTRO_ACCELERATE_HOST_RFI_HPP

namespace astroaccelerate {

void rfi(int nsamp, int nchans, unsigned short **input_buffer);

} //namespace astroaccelerate
  
#endif

