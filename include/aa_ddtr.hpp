#ifndef ASTRO_ACCELERATE_DDTR_HPP
#define ASTRO_ACCELERATE_DDTR_HPP

#include <iostream>
#include <utility>

#include "aa_filterbank_metadata.hpp"
#include "aa_ddtr_strategy.hpp"

namespace astroaccelerate {

    class aa_ddtr {
    
    private:
        bool _verbose;
        // Bookkeeping: stores where we are in de-dispersion plan
        int _old_range; 
        int _current_range;
	    int _old_timechunk;
        int _current_timechunk;
	
        float _processed_time;
        size_t _processed_samples; //inc = number of processed time samples so far.
    
        aa_ddtr_strategy _ddtr_strategy;
        aa_filterbank_metadata _filterbank_metadata;
    
    public:
        aa_ddtr(void) = delete;

        // NOTE: Should we make aa_filterbank_metadata part of aa_ddtr_strategy?
        aa_ddtr(aa_ddtr_strategy const& ddtr_strategy, aa_filterbank_metadata const& filterbank_metadata);
        ~aa_ddtr() = default;
       
        bool end_of_ddtr();
 

        std::pair<int, int> dedisperse_next_chunk(float *d_DDTR_output, unsigned short *d_DDTR_input, unsigned short *h_input_buffer);
        
        void save_dedispersed(float *d_DDTR_output,  size_t outsize);

        void save_filterbank(unsigned short *h_input);

        size_t processed_samples(void) const {
            return _processed_samples;
        } 

        int current_timechunk(void) const {
            return _current_timechunk;
        }

        int current_range(void) const {
            return _current_range;
        }

    };

} // namespace astroaccelerate

#endif
