#ifndef ASTRO_ACCELERATE_SPS_PIPELINE_HPP
#define ASTRO_ACCELERATE_SPS_PIPELINE_HPP

void run_single_pulse_search(const aa_filterbank_metadata &filterbank_data, std::vector<aa_ddtr_plan::dm> dm_ranges,  unsigned short *input_data, float *output_data);

void run_single_pulse_search(const aa_filterbank_metadata &filterbank_data, std::vector<aa_ddtr_plan::dm> dm_ranges,  std::vector<unsigned short> input_data, float *output_data);


#endif /* ASTRO_ACCELERATE_SPS_PIPELINE_HPP */