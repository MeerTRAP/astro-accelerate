#ifndef __ASTROACCELERATE_GETUSERINPUT_H_
#define __ASTROACCELERATE_GETUSERINPUT_H_

void get_user_input(FILE **fp, int argc, char *argv[], DDTR_Plan *DDTR_plan, AA_Parameters *AA_params, MSD_Parameters *MSD_params, SPS_Parameters *SPS_params, PRS_Parameters *PRS_params, FDAS_Parameters *FDAS_params);
#endif
