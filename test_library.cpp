#include "AstroAccelerate.h"
//#include "DedispersionStrategy.h"

#include <iostream>

using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    
    cout << "We're testing the AA library now!" << endl;

    astroaccelerate::DedispersionStrategy dedisp_strategy;
    astroaccelerate::AstroAccelerate<unsigned short> astro_accelerate(dedisp_strategy);

    return 0;
}

