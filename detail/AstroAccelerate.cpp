#include <algorithm>
#include <chrono>
#include <fstream>
#include <string>

#include "AstroAccelerate.h"
#include "aa_ddtr_plan.hpp"
#include "aa_ddtr_strategy.hpp"
#include "aa_ddtr.hpp"

#include "aa_sps_plan.hpp"
#include "aa_sps_strategy.hpp"
#include "aa_sps.hpp"
#include "errors.hpp"

#include <utility>

namespace astroaccelerate {

    template<typename AstroAccelerateParameterType>
    AstroAccelerate<AstroAccelerateParameterType>::AstroAccelerate(DedispersionStrategy const& dedispersion_strategy)
                                : _filterbank_metadata(dedispersion_strategy)
                                , _ddtr_plan(dedispersion_strategy)
                                , _ddtr_strategy(dedispersion_strategy)
                                , _sps_plan(dedispersion_strategy)
                                , _sps_strategy(_ddtr_strategy, _sps_plan, _filterbank_metadata)
                                , _verbose(false)
                                , _t_processed(_ddtr_strategy.t_processed())
                                , _ndms(_ddtr_strategy.ndms())
                                , _nboots(dedispersion_strategy.get_nboots())
                                , _ntrial_bins(dedispersion_strategy.get_ntrial_bins())
                                , _navdms(dedispersion_strategy.get_navdms())
                                , _narrow(dedispersion_strategy.get_narrow())
                                , _wide(dedispersion_strategy.get_wide())
                                , _nsearch(dedispersion_strategy.get_nsearch())
                                , _aggression(dedispersion_strategy.get_aggression())
    {
        _verbose = true;
        
        _multi_file = 0;
        _enable_debug = 0;
        _enable_analysis = 1;
        _enable_periodicity = 0;
        _enable_acceleration = 0;
        _enable_fdas_custom_fft = 1;
        _enable_fdas_inbin = 0;
        _enable_fdas_norm = 1;
        _output_dmt = 0;
        _enable_zero_dm = 0;
        _enable_zero_dm_with_outliers = 0;
        _enable_rfi = 0;
        _candidate_algorithm = 0;
        _enable_sps_baselinenoise = 0;
        _inc = 0;
        _tsamp_original = _tsamp;
        _gpu_input_size = 0;
        _d_input = nullptr;
        _gpu_output_size = 0;
        _d_output = nullptr;

        if (_verbose) {
            _ddtr_strategy.print_ddtr_strategy();
        }

    }

    template<typename AstroAccelerateParameterType>
    long int AstroAccelerate<AstroAccelerateParameterType>::get_processed_time()
    {
        return _inc;
    }

    template<typename AstroAccelerateParameterType>
    void AstroAccelerate<AstroAccelerateParameterType>::allocate_memory_gpu()
    {
        // NOTE: First time chunk of the first range is guaranteed to be the largest one, able to hold output data from other ranges and time chunks, wchich can only be either the same size or smaller
        int time_samps = _t_processed[0][0] + _ddtr_strategy.maxshift();

        _gpu_input_size = (size_t)time_samps * (size_t)_ddtr_strategy.nchans() * sizeof(unsigned short);

        if (_verbose) {
            std::cout << "Allocating " << (float)_gpu_input_size / 1000.0 / 1000.0 << "MB GPU memory input buffer" << std::endl
                        << "This will contain " << time_samps << " time samples (" << _t_processed[0][0] << " + " << _ddtr_strategy.maxshift() << ") for " << _ddtr_strategy.nchans() << " frequency channels." << std::endl;
        }

        cudaError_t rc1 = ( cudaMalloc((void **)&_d_input, _gpu_input_size) );
        if (rc1 != cudaSuccess)
        {
            throw std::bad_alloc();
        }

        try
        {
            if (_ddtr_strategy.nchans() < _ddtr_strategy.max_ndms())
            {
                _gpu_output_size = (size_t)time_samps * (size_t)_ddtr_strategy.max_ndms() * sizeof(float);
            }
            else
            {
                _gpu_output_size = (size_t)time_samps * (size_t)_ddtr_strategy.nchans() * sizeof(float);
            }
        }
        catch(...)
        {
            cudaFree(_d_input);
            throw;
        }

        cudaError_t rc2 = ( cudaMalloc((void **)&_d_output, _gpu_output_size) );
        if (rc2 != cudaSuccess)
        {
            cudaFree(_d_input);
            throw std::bad_alloc();
        }
        try
        {
            cudaMemset(_d_output, 0, _gpu_output_size);
        }
        catch(...)
        {
            cudaFree(_d_input);
            cudaFree(_d_output);
            throw;
        }
    }

    template<typename AstroAccelerateParameterType>
    void AstroAccelerate<AstroAccelerateParameterType>::run_dedispersion_sps(unsigned device_id
                            ,unsigned char *input_buffer
                            ,DmTime<float> &output_buffer
                            ,std::vector<float> &output_sps
                            )
    {

        unsigned short *input_buffer_cast = nullptr;
        size_t inputsize = _filterbank_metadata.nsamples() * _ddtr_strategy.nchans() * sizeof(unsigned short);
        input_buffer_cast = (unsigned short *) malloc(inputsize);
        if (input_buffer_cast == nullptr)
        {
            throw std::bad_alloc();
        }

        try
        {
            std::chrono::time_point<std::chrono::steady_clock> cast_start;
            std::chrono::time_point<std::chrono::steady_clock> cast_end;
            double cast_elapsed = 0.0;

            cast_start = std::chrono::steady_clock::now();

            size_t input_samples = static_cast<size_t>(_filterbank_metadata.nsamples())
                                    * static_cast<size_t>(_ddtr_startegy.nchans());

            for (size_t isample = 0; isample < input_samples; ++isample)
            {
                input_buffer_cast[isample] = (unsigned short)(input_buffer[isample]);
            }

            cast_end = std::chrono::steady_clock::now();
            cast_elapsed = std::chromo::duration<double>(cast_end - cast_start).count();
            std::cout << "TIME: Casting took " << cast_elapsed << "s to run\n";

            std::chrono::time_point<std::chrono::steady_clock> searchstart;
            std::chrono::time_point<std::chrono::steady_clock> searchend;
            double searchelapsed = 0.0;

            searchstart = std::chrono::steady_clock::now();

                    // call dd + sps
                    run_dedispersion_sps(device_id
                            ,input_buffer_cast
                            ,output_buffer
                            ,output_sps
                            );
            searchend = std::chrono::steady_clock::now();
            searchelapsed = std::chrono::duration<double>(searchend - searchstart).count();
            std::cout << "TIME: SPS took " << searchelapsed << "s to run" << std::endl;
 
        }
        catch(...)
        {
            free(input_buffer_cast);
            throw;
        }

        // free mem
        free(input_buffer_cast);
    }

    template<typename AstroAccelerateParameterType>
    void AstroAccelerate<AstroAccelerateParameterType>::run_dedispersion_sps(unsigned device_id
                            ,unsigned short *input_buffer
                            ,DmTime<float> &output_buffer
                            ,std::vector<float> &output_sps
                            )
    {

        // NOTE: We currently have less than ideal situation that AstroAccelerate object is created every time new chunk is sent by Cheetah
        // TODO: Need to write implementation that creates it and parses the DedispersionStrategy only once per run
        cudaSetDevice(device_id);

        allocate_memory_gpu();

        aa_ddtr ddtr(_ddtr_strategy, _filterbank_metadata);
        aa_sps sps(_sps_strategy);

        float tstart_local = 0.0f;
	    size_t processed_samples = 0; 

        try {
            int maxshift_original = _ddtr_strategy.maxshift();

            size_t max_peak_size = 0;

            for (int t = 0; t < _ddtr_strategy.num_tchunks(); t++) {
                for (int dm_range = 0; dm_range < _ddtr_plan.ranges(); dm_range++) {
                    max_peak_size += (size_t) ( _ndms[dm_range]*_t_processed[dm_range][t] / 2 );
                }
            }

            output_sps.resize(max_peak_size * 4);

            // NOTE: This is not ideal - we allocate memory for the worst case scenario
            float *candidates = (float*)malloc(4 * max_peak_size * sizeof(float));
            
            GpuTimer timer;
            timer.Start();

            std::pair<int, int> current_chunk;
            while(!ddtr.end_of_ddtr()) {
                // NOTE: current_chunk.first is range current_chunk.second is time chunk
                current_chunk = ddtr.dedisperse_next_chunk(_d_output, _d_input, input_buffer);
                cudaCheckError(cudaGetLastError());
                sps.update_details(current_chunk);
                sps.run_sps(_d_output, candidates, current_chunk);
                cudaCheckError(cudaGetLastError());
                processed_samples = ddtr.processed_samples();
                sps.convert_candidates(candidates, output_sps, current_chunk, _ddtr_strategy, processed_samples, _filterbank_metadata.tsamp());
            }


            std::chrono::time_point<std::chrono::steady_clock> sortstart;
            std::chrono::time_point<std::chrono::steady_clock> sortend;
            double sortelapsed = 0.0;
            sortstart = std::chrono::steady_clock::now();
            sps.sort_candidates(output_sps);

            sortend = std::chrono::steady_clock::now();
            sortelapsed = std::chrono::duration<double>(sortend - sortstart).count();
            std::cout << "TIME: SPS sorting took " << sortelapsed << "s to run" << std::endl;

            processed_samples = ddtr.processed_samples();
            tstart_local = processed_samples * _filterbank_metadata.tsamp();
            
            timer.Stop();
            float elapsed_time = timer.Elapsed() / 1000;

            if(_verbose) {
                printf("\n\n === OVERALL DEDISPERSION THROUGHPUT INCLUDING SYNCS AND DATA TRANSFERS ===\n");
                printf("\n(Performed Brute-Force Dedispersion: %g (GPU estimate)",  elapsed_time);
                printf("\nAmount of telescope time processed: %f", tstart_local);
                printf("\nNumber of samples processed: %ld", processed_samples);
                printf("\nReal-time speedup factor: %lf\n", ( tstart_local ) / elapsed_time);
            }

 
        }
        catch(...) {
                cudaFree(_d_input);
                cudaFree(_d_output);
                throw;
        }

        //free(out_tmp);
        cudaFree(_d_input);
        cudaFree(_d_output);
    }

} // namespace astroaccelerate
