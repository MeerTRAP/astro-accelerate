#include <fstream>
#include <iostream>
#include <utility>
#include <thrust/sort.h>

#include "aa_ddtr_strategy.hpp"
#include "aa_sps.hpp"
#include "aa_sps_analysis.hpp"

namespace astroaccelerate {

    struct sps_cmp {
        __global__ bool operator()(const aa_cand &lhs, const aa_cand &rhs) {
            return lhs.time < rhs.time;
        }
    };

    aa_sps::aa_sps(aa_sps_strategy const& sps_strategy)
        : _sps_strategy(sps_strategy)
        , _candidate_list(NULL)
        , _number_candidates(0)
        , _previous_candidates(0)
        , _verbose(false)
    {   
        #ifdef VERBOSE
            _verbose = true;
        #endif

    }

    /**
     * @brief Updates the processing details for current range and time chunk 
     * 
     * Makes sure correct number of samples is processed and correct boxcar widths are used
     */
    void aa_sps::update_details(std::pair<int, int> current_chunk) {
        _sps_strategy.generate_processing_details(current_chunk.first, current_chunk.second);
    }

    int aa_sps::run_sps(float *d_input, float *candidate_list, std::pair<int, int> current_chunk) {
        if (d_input == NULL) {
            std::cerr << "ERROR: input data pointer is NULL!" << std::endl;
            return 1;
        }
        _candidate_list = candidate_list;
        _previous_candidates = _number_candidates;

        analysis_GPU(_verbose, d_input, _candidate_list, _number_candidates, _sps_strategy, current_chunk);
    
        return(0);
    }

    void aa_sps::convert_candidates(float *candidate_list, std::vector<float> &output_candidates, std::pair<int, int> const &current_chunk, aa_ddtr_strategy const &ddtr_strategy, size_t processed_samples, float sampling_time) {
        size_t new_candidates = _number_candidates - _previous_candidates;
        if (_verbose) {
            std::cout << "Total candidates: " << _number_candidates << std::endl;
            std::cout << new_candidates << " new candidates..." << std::endl;
        }

        float *candidates_start = candidate_list + _previous_candidates * 4;

        int current_range = current_chunk.first;

        size_t skip_cands = _previous_candidates * 4;
        size_t cand_position = 0;

        // NOTE: All the values stored in candidate_list have any averaging taken into accout and are multiplied by correct factors
        // NOTE: All we're doing here is converting to physical units
        for (size_t icand = 0; icand < new_candidates; ++icand) {
            // DM in pc cm^-3
            cand_position = skip_cands + icand * 4;
            output_candidates[cand_position + 0] =  (float)candidates_start[icand * 4 + 0] * (ddtr_strategy.dmstep())[current_range] + (ddtr_strategy.dmlow())[current_range];
            // Start time in seconds
            output_candidates[cand_position + 1] = (float)(candidates_start[icand * 4 + 1] * (ddtr_strategy.inbin())[current_range] + processed_samples) * sampling_time;
            // SNR
            output_candidates[cand_position + 2] = (float)candidates_start[icand * 4 + 2];
            // Width in seconds
            output_candidates[cand_position + 3] = (float)candidates_start[icand * 4 + 3] * (ddtr_strategy.inbin())[current_range] * sampling_time;
        }
    }
       

    void aa_sps::sort_candidates(std::vector<float> &output_candidates) {

        std::vector<aa_cand> output_candidates_tmp;
        output_candidates_tmp.reserve(_number_candidates);

        std::cout << "Final total number of candidates: " << _number_candidates << std::endl;

        for (size_t icand = 0; icand < _number_candidates; ++icand) {
            output_candidates_tmp.emplace_back(aa_cand{output_candidates[icand * 4 + 1], // TIME
                                            output_candidates[icand * 4 + 0], // DM
                                            output_candidates[icand * 4 + 2], // SNR
                                            output_candidates[icand * 4 + 3]});
        }

        thrust::sort(output_candidates_tmp.begin(), output_candidates_tmp.end(), sps_cmp());

        output_candidates.clear();
        std::cout << "Total number of candidates: " << _number_candidates << std::endl;
        output_candidates.resize(_number_candidates * 4);

        for (size_t icand = 0; icand < _number_candidates; ++icand) {
            // DM in pc cm^-3
            output_candidates[icand * 4 + 0] = output_candidates_tmp.at(icand).dm;
            // Start time in seconds
            output_candidates[icand * 4 + 1] = output_candidates_tmp.at(icand).time;
            // SNR
            output_candidates[icand * 4 + 2] = output_candidates_tmp.at(icand).snr;
            // Width in seconds
            output_candidates[icand * 4 + 3] = output_candidates_tmp.at(icand).width;
        }

    }
} // namespace astroaccelerate
