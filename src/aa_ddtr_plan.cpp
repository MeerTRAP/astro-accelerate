#include "aa_ddtr_plan.hpp"

namespace astroaccelerate {

    aa_ddtr_plan::aa_ddtr_plan(DedispersionStrategy const& dedispersion_strategy)
                    : user_dm_low(NULL)
                    , user_dm_high(NULL)
                    , user_dm_step(NULL)
                    , inBin(NULL)
                    , outBin(NULL)
                    , _n_ranges(dedispersion_strategy.get_range())
    {
        
        int error=0;
        // user ranges
        if(user_dm_low) free(user_dm_low);
        user_dm_low   = (float *) malloc( _n_ranges*sizeof(float) );
        if(user_dm_low==NULL) error++;

        if(user_dm_high) free(user_dm_high);
        user_dm_high  = (float *) malloc( _n_ranges*sizeof(float) );
        if(user_dm_high==NULL) error++;

        if(user_dm_step) free(user_dm_step);
        user_dm_step  = (float *) malloc( _n_ranges*sizeof(float) );
        if(user_dm_step==NULL) error++;

        if(inBin) free(inBin);
        inBin         = (int *) malloc( _n_ranges*sizeof(int) );
        if(inBin==NULL) error++;

        if (outBin) free(outBin);
        outBin = (int *)malloc(_n_ranges * sizeof(int));
        

        for (int irange = 0; irange < _n_ranges; ++irange) {
            user_dm_low[irange] = *(dedispersion_strategy.get_user_dm_low().data() + irange);
            user_dm_high[irange] = *(dedispersion_strategy.get_user_dm_high().data() + irange);
            user_dm_step[irange] = *(dedispersion_strategy.get_user_dm_step().data() + irange);
            inBin[irange] = *(dedispersion_strategy.get_in_bin() + irange);
            outBin[irange] = *(dedispersion_strategy.get_out_bin() + irange);
        }
    }

    int aa_ddtr_plan::ranges(void) const {
        return _n_ranges;
    }        

} // namespace astroaccelerate