#include "aa_ddtr_strategy.hpp"

namespace astroaccelerate {

    aa_ddtr_strategy::aa_ddtr_strategy(DedispersionStrategy const& dedispersion_strategy)
                        : _dm_low(NULL)
		                , _dm_high(NULL)
		                , _dm_step(NULL)
		                , _dm_shifts(NULL)
                        , _enable_zero_dm(true)
                        , _enable_zero_dm_outliers(false)
                        , _enable_rfi(false)
                        , _in_bin(NULL)
                        , _out_bin(NULL)
                        , _ndms(NULL)
                        , _t_processed(NULL)
                        , _max_dm(dedispersion_strategy.get_max_dm())
                        , _max_ndms(dedispersion_strategy.get_max_ndms())
                        , _maxshift(dedispersion_strategy.get_maxshift())
                        , _total_ndms(dedispersion_strategy.get_total_ndms())
                        , _max_samps(dedispersion_strategy.get_max_samps())
                        , _num_tchunks(dedispersion_strategy.get_num_tchunks())
                        , _n_ranges(dedispersion_strategy.get_range())
                        , _nchans(dedispersion_strategy.get_nchans())
    {
        _dm_low = new float[_n_ranges];
        _dm_high = new float[_n_ranges];
        _dm_step = new float[_n_ranges];
        _dm_shifts = new float[_nchans];
        _ndms = new int[_n_ranges];
        _in_bin = new int[_n_ranges];
        _out_bin = new int[_n_ranges];

        int **t_processed_tmp = dedispersion_strategy.get_t_processed();

        _t_processed = new int*[_n_ranges];

        // NOTE: Deep copy just to be safe
        for (int irange = 0; irange < _n_ranges; ++irange) {
            _dm_low[irange] = *(dedispersion_strategy.get_dm_low() + irange);
            _dm_high[irange] = *(dedispersion_strategy.get_dm_high() + irange);
            _dm_step[irange] = *(dedispersion_strategy.get_dm_step() + irange);
            _ndms[irange] = *(dedispersion_strategy.get_ndms() + irange);
            _in_bin[irange] = *(dedispersion_strategy.get_in_bin() + irange);
            _out_bin[irange] = *(dedispersion_strategy.get_out_bin() + irange);
            _t_processed[irange] = new int[_num_tchunks];
            for (int ichunk = 0; ichunk < _num_tchunks; ++ichunk) {
                _t_processed[irange][ichunk] = t_processed_tmp[irange][ichunk];
            }
        }

        for (int ichan = 0; ichan < _nchans; ++ichan) {
            _dm_shifts[ichan] = *(dedispersion_strategy.get_dmshifts() + ichan);
        }

        
        is_setup = true;
    }

    aa_ddtr_strategy::aa_ddtr_strategy(aa_ddtr_strategy const& ddtr_strategy)
                        : _n_ranges(ddtr_strategy.ranges())
                        , _dm_low(NULL)
		                , _dm_high(NULL)
		                , _dm_step(NULL)
		                , _dm_shifts(NULL)
                        , _enable_zero_dm(ddtr_strategy._enable_zero_dm)
                        , _enable_zero_dm_outliers(ddtr_strategy._enable_zero_dm_outliers)
                        , _enable_rfi(ddtr_strategy._enable_rfi)
                        , _in_bin(NULL)
                        , _out_bin(NULL)
                        , _ndms(NULL)
                        , _t_processed(NULL)
                        , _nchans(ddtr_strategy.nchans())
                        , _max_dm(ddtr_strategy.maxdm())
                        , _max_ndms(ddtr_strategy.max_ndms())
                        , _maxshift(ddtr_strategy.maxshift())
                        , _total_ndms(ddtr_strategy.total_ndms())
                        , _max_samps(ddtr_strategy.max_samps())
                        , _num_tchunks(ddtr_strategy.num_tchunks())
    {
        _dm_low = new float[_n_ranges];
        _dm_high = new float[_n_ranges];
        _dm_step = new float[_n_ranges];
        _dm_shifts = new float[_nchans];
        _ndms = new int[_n_ranges];
        _in_bin = new int[_n_ranges];
        _out_bin = new int[_n_ranges];

        int **t_processed_tmp = ddtr_strategy.t_processed();

        _t_processed = new int*[_n_ranges];

        for (int irange = 0; irange < _n_ranges; ++irange) {
            _dm_low[irange] = *(ddtr_strategy.dmlow() + irange);
            _dm_high[irange] = *(ddtr_strategy.dmhigh() + irange);
            _dm_step[irange] = *(ddtr_strategy.dmstep() + irange);
            _ndms[irange] = *(ddtr_strategy.ndms() + irange);
            _in_bin[irange] = *(ddtr_strategy.inbin() + irange);
            _out_bin[irange] = *(ddtr_strategy.outbin() + irange);
            _t_processed[irange] = new int[_num_tchunks];
            for (int ichunk = 0; ichunk < _num_tchunks; ++ichunk) {
                _t_processed[irange][ichunk] = t_processed_tmp[irange][ichunk];
            }
        }

        for (int ichan = 0; ichan < _nchans; ++ichan) {
            _dm_shifts[ichan] = *(ddtr_strategy.dmshifts() + ichan);
        }

        is_setup = true;
    }

    aa_ddtr_strategy::~aa_ddtr_strategy()
    {
        for (int irange = 0; irange < _n_ranges; ++irange) {
            delete [] _t_processed[irange];
        }
        delete [] _t_processed;
        delete [] _out_bin;
        delete [] _in_bin;
        delete [] _ndms;
        delete [] _dm_shifts;
        delete [] _dm_step;
        delete [] _dm_high;
        delete [] _dm_low;
    }

    void aa_ddtr_strategy::print_ddtr_strategy(void) const {
        std::cout << "Dedispersion strategy: " << std::endl
                    << "\tnumber of dedispersion ranges: " << _n_ranges << std::endl
                    << "\tRange #\tDM Low\tDM High\tDM Step\t# DMs\tIn Bin\tOut Bin" << std::endl;
                    for (int irange = 0; irange < _n_ranges; ++irange) {
                        std::cout << "\t" << irange << "\t" << _dm_low[irange] << "\t" << _dm_high[irange] << "\t" << _dm_step[irange]
                                    << "\t" << _ndms[irange] << "\t" << _in_bin[irange] << "\t" << _out_bin[irange] << std::endl;
                    }
                    std::cout << "\tmax shift: " << _maxshift << std::endl
                    << "\tnumber of time chunks: " << _num_tchunks << std::endl
                    << "\tRange #\tTime decomposition" << std::endl;
                    for (int irange = 0; irange < _n_ranges; ++irange) {
                        std::cout << "\t" << irange << "\t";
                        for (int ichunk = 0; ichunk < _num_tchunks; ++ichunk) {
                            std::cout << _t_processed[irange][ichunk] << "\t";
                        }
                        std::cout << std::endl;
                    }
    }

} // namespace astroaccelerate
