#include <algorithm>
#include <chrono>
#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>

#include "aa_channel_mask.cuh"

namespace astroaccelerate {

    __global__ void thresholdMask(unsigned short * __restrict__ data, int nchans, int nsamps, unsigned short threshold) {

        // NOTE: This assumes the data has already been transposed, i.e. TF order
    
        for (int tid = blockIdx.x; tid < nsamps; tid += gridDim.x) {
            int nreps = nchans / blockDim.x;
            // NOTE: Median is saved in the first channel
            //float median = data[tid * nchans];
    
            int full_idx = tid * nchans + threadIdx.x;

            unsigned short median = data[tid * nchans];
    
            for (int irep = 0; irep < nreps; ++irep) {
                
                // NOTE: This approach gives the same result as running with if statement and is marginally faster
                short diff = data[full_idx] - median;
                short sign = ( ( ((short)abs(float(diff)) - threshold) & 0x8000) >> 15);
        
                data[full_idx] = (unsigned short)(data[full_idx] + (sign - 1) * diff);
                full_idx += blockDim.x;
            }
    
        }
    
    }

    void channel_mask(unsigned short *const d_input, const int nchans, const int nsamp) {

        // NOTE: This masking assumes we are working in the original FT data layout

        // Threads will deal with channels within the time sample
        // Blocks will deal with time samples
        // Note likely we will ever work with less than 1024 channels, but worth checkign anyway
        int threads = std::min(nchans, 1024);
        // TODO: Use occupancy API for that
        // This is a large number, but still smaller than the usual 'nsamp' approach
        int blocks = 32768;

        std::chrono::time_point<std::chrono::steady_clock> maskstart;
        std::chrono::time_point<std::chrono::steady_clock> maskend;
        double maskelapsed = 0.0;

        maskstart = std::chrono::steady_clock::now();

        thresholdMask<<<blocks, threads>>>(d_input, nchans, nsamp, 3 * 7);

        maskend = std::chrono::steady_clock::now();
        maskelapsed = std::chrono::duration<double>(maskend - maskstart).count();

        std::cout << "TIME: threshold masking took " << maskelapsed << "s to run" << std::endl;

    }

}
