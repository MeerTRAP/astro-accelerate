#include <fstream>
#include <sstream>

#include "cuda/helper_cuda.h"

#include "aa_bin_gpu.hpp"
#include "aa_channel_mask.cuh"
#include "aa_corner_turn.hpp"
#include "aa_ddtr.hpp"
#include "aa_dedisperse.hpp"
#include "aa_device_load_data.hpp"
#include "aa_zero_dm.hpp"
#include "aa_zero_dm_outliers.hpp"
#include "aa_filterbank_metadata.hpp"


#include "errors.hpp"
#include "device_rfi.hpp"

namespace astroaccelerate {

    aa_ddtr::aa_ddtr(aa_ddtr_strategy const& ddtr_strategy, aa_filterbank_metadata const& filterbank_metadata)
        : _old_range(-1)
        , _current_range(-1)
        , _old_timechunk(-1)
        , _current_timechunk(-1)
        , _processed_samples(0)
        , _processed_time(0.0f)
        , _verbose(false)
        // NOTE: That should have copy constructor properly working now
        , _ddtr_strategy(ddtr_strategy)
        , _filterbank_metadata(filterbank_metadata)
    {
        #ifdef VERBOSE
            _verbose = true;
        #endif
    }
    
    bool aa_ddtr::end_of_ddtr()  
    {
        if ((_old_timechunk == -1) && (_old_range == -1) && (_ddtr_strategy.ranges() != 0) && (_ddtr_strategy.num_tchunks() !=0)) {
            if (_verbose) {
                std::cout << "Beginning of the DDTR..." << std::endl;
            }
            return false;
        } else if (_current_timechunk != _ddtr_strategy.num_tchunks()) {
            if (_verbose) {
                std::cout << "DDTR not done yet..." << std::endl;
            }
            return false;
        } else if (( _ddtr_strategy.ranges() == 0) && (_ddtr_strategy.num_tchunks() == 0 )) {
            std::cerr << "ERROR: Invalid DDTR input! Please check your strategy!" << std::endl;
            return true;
        } else if (_current_timechunk == _ddtr_strategy.num_tchunks()) {
            if (_verbose) {
            std::cout << "End of DDTR..." << std::endl;
            }
            _processed_samples += (_ddtr_strategy.t_processed())[0][_old_timechunk];
            _processed_time = _filterbank_metadata.tsamp() * _processed_samples;
            return true;
        } else {
            std::cout << "ERROR: DDTR conditions not met for some reason! Please check the processing logs!" << std::endl;
            return true;
        }
    }
    
    std::pair<int, int> aa_ddtr::dedisperse_next_chunk(float *d_DDTR_output, unsigned short *d_DDTR_input, unsigned short *h_input_buffer) {
        
        if ((_old_timechunk == -1) && (_old_range == -1)) {
            if (_verbose) {
                std::cout << "First dedispersion run..." << std::endl;
                
            }
            _old_timechunk = -1;
            _current_timechunk = 0;
            _old_range = 0;
            _current_range = 0;
        } else if (_current_range == 0) {
            _processed_samples += (_ddtr_strategy.t_processed())[0][_old_timechunk];
            _processed_time = _filterbank_metadata.tsamp() * _processed_samples;
            if (_verbose) {
                std::cout << "Amount of telescope time fully processed: " << _processed_time << std::endl;
            }
        }
        
        if (_verbose) {
            std::cout << "Dedispersing new chunk at range " << _current_range << " and time chunk " << _current_timechunk << std::endl;
            std::cout << "Previous range and time chunk: " << _old_range << " " << _old_timechunk << std::endl;
            std::cout << "DM range: " << _ddtr_strategy.dmlow()[_current_range] << "--" << _ddtr_strategy.dmhigh()[_current_range]
                        << ", " << _ddtr_strategy.ndms()[_current_range] << " DM trials with step of " << _ddtr_strategy.dmstep()[_current_range] << std::endl;
        }
        
        // TODO: This shouldn't be the case in general - we should be able to start with any factor we want
        if ((_current_range == 0) && (_ddtr_strategy.inbin()[_current_range] > 1)) {
            std::cerr << "ERROR: first range is not in full time resolution (has input binning factor > 1)!" << std::endl;
            return std::make_pair(-1, -1);
        } else if ((_current_range != 0) && (_ddtr_strategy.inbin()[_current_range] != (2 * _ddtr_strategy.inbin()[_old_range]))) {
            std::cerr << "ERROR: unsupported time resolution change between ranges " << _old_range << " and " << _current_range << ": "
                        << _ddtr_strategy.inbin()[_old_range] << " -> " << _ddtr_strategy.inbin()[_current_range] << std::endl;
        } // else it's all fine and don't print anything 
        
        size_t local_maxshift = _ddtr_strategy.maxshift() / _ddtr_strategy.inbin()[_current_range];
        float  local_tsamp    = _filterbank_metadata.tsamp() * _ddtr_strategy.inbin()[_current_range];
        
        
        // NOTE: Run the algorithm cleaning only when the first range is being processed - the same, cleaned data is used for other ranges in the same time chunk
        if( _current_range == 0 ) {

            if (_verbose) {
                std::cout << "t_processed: " << (_ddtr_strategy.t_processed())[0][_current_timechunk] << ", time chunk: " << _current_timechunk << ", max shift: " << local_maxshift << std::endl;
            }

            // This should be split into two functions
            load_data(-1, _ddtr_strategy.inbin(), d_DDTR_input, &h_input_buffer[_processed_samples * _filterbank_metadata.nchans()], (int)(_ddtr_strategy.t_processed())[0][_current_timechunk], (int)local_maxshift, (int)_filterbank_metadata.nchans(), _ddtr_strategy.dmshifts());
            
            checkCudaErrors(cudaGetLastError());
            
            // Extra channel masking
            //if (_ddtr_strategy.enable_channel_masking()) {
            /*if (true) {
                channel_mask(d_DDTR_input, (int)_filterbank_metadata.nchans(), (int)((_ddtr_strategy.t_processed())[0][_current_timechunk] + local_maxshift));
            }*/

            if (_ddtr_strategy.enable_zero_dm()) {
                // ideally do as a callback
                zero_dm(d_DDTR_input, (int)_filterbank_metadata.nchans(), (int)((_ddtr_strategy.t_processed())[0][_current_timechunk] + local_maxshift), _filterbank_metadata.nbits());
            }
            
            checkCudaErrors(cudaGetLastError());
            
            if (_ddtr_strategy.enable_zero_dm_outliers()) {
                zero_dm_outliers(d_DDTR_input, (int)_filterbank_metadata.nchans(), (int)((_ddtr_strategy.t_processed())[0][_current_timechunk] + local_maxshift));
            }
            
            checkCudaErrors(cudaGetLastError());
        
            corner_turn(d_DDTR_input, d_DDTR_output, (int)_filterbank_metadata.nchans(), (int)((_ddtr_strategy.t_processed())[0][_current_timechunk] + local_maxshift));

            

            checkCudaErrors(cudaGetLastError());
            
            if (_ddtr_strategy.enable_rfi()) {
                rfi_gpu(d_DDTR_input, (int)_filterbank_metadata.nchans(), (int)((_ddtr_strategy.t_processed())[0][_current_timechunk] + local_maxshift));
            }
        
            checkCudaErrors(cudaGetLastError());
        }

        cudaDeviceSynchronize();
        
        // NOTE: This one doesn't load the data - just updates constants for dedispersion kernel
        load_data(_current_range, _ddtr_strategy.inbin(), d_DDTR_input, &h_input_buffer[_processed_samples * _filterbank_metadata.nchans()], (int)(_ddtr_strategy.t_processed())[_current_range][_current_timechunk], (int)local_maxshift, (int)_filterbank_metadata.nchans(), _ddtr_strategy.dmshifts());
        
        checkCudaErrors(cudaGetLastError());
        if (_ddtr_strategy.inbin()[_current_range] == (2 * _ddtr_strategy.inbin()[_old_range])) {
            std::cout << "Binning the original data by a factor of " << _ddtr_strategy.inbin()[_current_range] << "..." << std::endl;
            bin_gpu(d_DDTR_input, d_DDTR_output, (int)_filterbank_metadata.nchans(), (int)((_ddtr_strategy.t_processed())[_current_range - 1][_current_timechunk]  + local_maxshift * (_ddtr_strategy.inbin())[_current_range]));
        }
       
        cudaDeviceSynchronize(); 
        cudaCheckError(cudaGetLastError());
        
        dedisperse(_current_range, (int)(_ddtr_strategy.t_processed())[_current_range][_current_timechunk], _ddtr_strategy.inbin(), _ddtr_strategy.dmshifts(), d_DDTR_input, d_DDTR_output,
        (int)_filterbank_metadata.nchans(), &local_tsamp, _ddtr_strategy.dmlow(), _ddtr_strategy.dmstep(), _ddtr_strategy.ndms(), _filterbank_metadata.nbits(), 0); // Last parameter is failsafe - currently assuming we can use shared memory version

        cudaDeviceSynchronize();
        cudaCheckError(cudaGetLastError());
        _old_range = _current_range;
        _old_timechunk = _current_timechunk;
        _current_range++;
        
        // NOTE: Processed all the ranges for current time chunk
        if(_current_range == _ddtr_strategy.ranges()) {
            if (_verbose) {
                std::cout << "Processed all ranges for this timechunk, moving to new time chunk..." << std::endl;
            }
            _current_range = 0;
            _old_timechunk = _current_timechunk;
            _current_timechunk++;
        }
        
        return std::make_pair(_old_range, _old_timechunk);
    }

    void aa_ddtr::save_filterbank(unsigned short *h_input) {
        std::stringstream ss;
        std::string filename;

        ss << "aa_filterbank_chunk_" << _current_timechunk << "_range_" << _current_range << ".dat";
        filename = ss.str();
        std::ofstream filout(filename.c_str(), std::ios_base::binary);

        size_t outsize = _filterbank_metadata.nsamples() * _filterbank_metadata.nchans() * sizeof(unsigned short);

        filout.write(reinterpret_cast<char*>(h_input), outsize);
        filout.close();

    }

    void aa_ddtr::save_dedispersed(float *d_DDTR_output, size_t outsize) {
        std::stringstream ss;
        std::string filename;

        ss << "aa_dedispersed_chunk_" << _current_timechunk << "_range_" << _current_range << ".dat";

        filename = ss.str();

        std::ofstream dedispout(filename.c_str(), std::ios_base::binary);

        float *h_ddtr_output = new float[outsize / sizeof(float)];

        cudaMemcpy(h_ddtr_output, d_DDTR_output, outsize, cudaMemcpyDeviceToHost);

        dedispout.write(reinterpret_cast<char*>(h_ddtr_output), outsize);

        delete [] h_ddtr_output;

        dedispout.close();

    }

} // namespace astroaccelerate
