#include <iostream>

#include "aa_sps_strategy.hpp"

namespace astroaccelerate {

    aa_sps_strategy::aa_sps_strategy(aa_ddtr_strategy const& ddtr_strategy, aa_sps_plan const& sps_plan, aa_filterbank_metadata const& filterbank_metadata)
                        : _candidate_algorithm(sps_plan.algorithm())
                        , _current_time_samples(0)
                        , _filterbank_metadata(filterbank_metadata)
                        , _iterations(0)
                        , _max_candidates(0)
                        , _max_boxcar_width_performed(0)
                        , _sigma_cutoff(sps_plan.threshold())
                        , _sigma_constant(sps_plan.sigma_constant())
                        , _decimate_bc_limits(sps_plan.decimate_limits())
                        , _nranges(ddtr_strategy.ranges())
                        , _ntchunks(ddtr_strategy.num_tchunks())
    {
        _ndms = new int[_nranges];
        _in_bin = new int[_nranges];
        _t_processed = new int*[_nranges];

        int **t_processed_tmp = ddtr_strategy.t_processed();

        for (int irange = 0; irange < _nranges; ++irange) {
            _ndms[irange] = *(ddtr_strategy.ndms() + irange);
            _in_bin[irange] = *(ddtr_strategy.inbin() + irange);
            _t_processed[irange] = new int[_ntchunks];
            for (int ichunk = 0; ichunk < _ntchunks; ++ichunk) {
                _t_processed[irange][ichunk] = t_processed_tmp[irange][ichunk];
            }
        }

        // NOTE: _max_candidates is a global variable now
        for (int irange = 0; irange < _nranges; irange++) {
            for (int ichunk = 0; ichunk < _ntchunks; ++ichunk) {
                // NOTE: Some places divide by 4, while other divide by 2
                // Keep the division by 2 for now to be safer
                _max_candidates += static_cast<size_t>( _ndms[irange] * _t_processed[irange][ichunk] / 2 );
            }
        }

        // NOTE: That now depends on the range - needs to take binning into account
        _max_boxcar_width_desired = static_cast<int>(sps_plan.max_boxcar_width_in_sec() / (_filterbank_metadata.tsamp()));
        
    }

    aa_sps_strategy::aa_sps_strategy(aa_sps_strategy const& sps_strategy)
                        : _boxcar_widths(sps_strategy._boxcar_widths) 
                        , _candidate_algorithm(sps_strategy._candidate_algorithm)
                        , _current_time_samples(sps_strategy._current_time_samples)
                        , _decimate_bc_limits(sps_strategy._decimate_bc_limits)
                        , _details(sps_strategy._details)
                        , _iterations(sps_strategy._iterations)
                        , _filterbank_metadata(sps_strategy._filterbank_metadata)
                        , _max_boxcar_width_desired(sps_strategy._max_boxcar_width_desired)
                        , _max_candidates(sps_strategy._max_candidates)
                        , _max_boxcar_width_performed(sps_strategy._max_boxcar_width_performed)
                        , _msdparams(sps_strategy._msdparams)
                        , _nranges(sps_strategy._nranges)
                        , _ntchunks(sps_strategy._ntchunks)
                        , _sigma_constant(sps_strategy._sigma_constant)
                        , _sigma_cutoff(sps_strategy._sigma_cutoff)

    {

        _ndms = new int[_nranges];
        _in_bin = new int[_nranges];
        _t_processed = new int*[_nranges];

        int **t_processed_tmp = sps_strategy._t_processed;

        for (int irange = 0; irange < _nranges; ++irange) {
            _ndms[irange] = sps_strategy._ndms[irange];
            _in_bin[irange] = sps_strategy._in_bin[irange];
            _t_processed[irange] = new int[_ntchunks];
            for (int ichunk = 0; ichunk < _ntchunks; ++ichunk) {
                _t_processed[irange][ichunk] = t_processed_tmp[irange][ichunk];
            }
        }

    }


    int aa_sps_strategy::get_boxcar_width(int element, std::vector<int> &widths) const {
        // NOTE: There is enough elements in the widths list
        if (element < widths.size()) {
            return widths.at(element);
        // NOTE: Not enough elements - get the last element
        } else if (widths.size() > 0) {
            return widths.back();
        // NOTE: Edge case - the widths vector is empty
        } else {
            return 0;
        }
    }

    void aa_sps_strategy::generate_processing_details(int current_range, int current_time) {

        int elements_per_block;
        int elements_rem;

        std::pair<int, size_t> width_limits;
        IterationDetails tmpdetails = {0};

        // NOTE: Need to remove previous processing details
        _details.clear();

        size_t dedispersed_samples = _t_processed[current_range][current_time];
        _current_time_samples = _t_processed[current_range][current_time];

        if(_max_boxcar_width_desired / _in_bin[current_range] > dedispersed_samples) { 
            width_limits = calculate_max_iterations(dedispersed_samples);
        } else {
            width_limits = calculate_max_iterations(_max_boxcar_width_desired / _in_bin[current_range]);
        }

        _iterations = width_limits.first;
        _max_boxcar_width_performed = width_limits.second;
        
        generate_boxcars_widths();

        if(_iterations > 0) {
            tmpdetails.shift = 0;
            tmpdetails.output_shift = 0;
            tmpdetails.start_taps = 0;
            tmpdetails.iteration = 0;

            tmpdetails.decimated_timesamples = dedispersed_samples;
            // NOTE: Each iteration decimates the data by a factor of 2 in the time dimension
            tmpdetails.dtm = (dedispersed_samples) >> (tmpdetails.iteration + 1);
            // NOTE: That creates an even number of decimated time samples
            tmpdetails.dtm = tmpdetails.dtm - (tmpdetails.dtm & 1);
            
            tmpdetails.number_boxcars = get_boxcar_width(0, _decimate_bc_limits);
            elements_per_block = PD_NTHREADS * 2 - tmpdetails.number_boxcars;
            tmpdetails.number_blocks = tmpdetails.decimated_timesamples / elements_per_block;
            elements_rem = tmpdetails.decimated_timesamples - tmpdetails.number_blocks * elements_per_block;

            if (elements_rem > 0) {
                tmpdetails.number_blocks++;
            }

            // TODO: What's the logic behind this equation?
            tmpdetails.unprocessed_samples = tmpdetails.number_boxcars + 6;

            if (tmpdetails.decimated_timesamples < tmpdetails.unprocessed_samples) {
                tmpdetails.number_blocks = 0;
            }

            tmpdetails.total_unprocessed = tmpdetails.unprocessed_samples;

            _details.push_back(tmpdetails);
        }

        for (int iiter = 1; iiter < _iterations; ++iiter) {
            tmpdetails.shift = tmpdetails.number_boxcars / 2;
            tmpdetails.output_shift = tmpdetails.output_shift + tmpdetails.decimated_timesamples;
            tmpdetails.start_taps = tmpdetails.start_taps + tmpdetails.number_boxcars * (1 << tmpdetails.iteration);
            tmpdetails.iteration = tmpdetails.iteration + 1;

            tmpdetails.decimated_timesamples = tmpdetails.dtm;
            tmpdetails.dtm = (dedispersed_samples) >> (tmpdetails.iteration + 1);
            tmpdetails.dtm = tmpdetails.dtm - (tmpdetails.dtm & 1);

            tmpdetails.number_boxcars = get_boxcar_width(tmpdetails.iteration, _decimate_bc_limits);
            elements_per_block = PD_NTHREADS * 2 - tmpdetails.number_boxcars;
            tmpdetails.number_blocks = tmpdetails.decimated_timesamples / elements_per_block;
            elements_rem = tmpdetails.decimated_timesamples - tmpdetails.number_blocks * elements_per_block;
            if (elements_rem > 0) {
                tmpdetails.number_blocks++;
            }

            tmpdetails.unprocessed_samples = tmpdetails.unprocessed_samples / 2 + tmpdetails.number_boxcars + 6;
            if (tmpdetails.decimated_timesamples < tmpdetails.unprocessed_samples) {
                tmpdetails.number_blocks = 0;
            }

            tmpdetails.total_unprocessed = tmpdetails.unprocessed_samples * (1 << tmpdetails.iteration);
            _details.push_back(tmpdetails);
        }

    }

    std::pair<int, size_t> aa_sps_strategy::calculate_max_iterations(int max_boxcar_width) {
        int iteration = 0;
        int max_boxcar_width_performed = 0;
        // TODO: This needs some careful condition checking - we can still end up with width greater than the number of time samples
        while (max_boxcar_width_performed < max_boxcar_width) {
            max_boxcar_width_performed += get_boxcar_width(iteration, _decimate_bc_limits) *  (1 << iteration);
            iteration++;
        }
        return std::make_pair(iteration, max_boxcar_width_performed);
    }

    void aa_sps_strategy::generate_boxcars_widths(){
        int current_decimation = 1;
        int width = 0;
        int f = 0;

        while (width < _max_boxcar_width_performed) {
            // NOTE: That loops over all possible widths within the current decimation
            for (int b = 0; b < get_boxcar_width(f, _decimate_bc_limits); b++) {
                width = width + current_decimation;
                _boxcar_widths.push_back(width);
            }
            current_decimation *= 2;
            f++;
        }
    }

    void aa_sps_strategy::print_sps_strategy(void) const {
        std::cout << "Single pulse search strategy: " << std::endl 
                    << "\tcandidate algorithm: " << (_candidate_algorithm == 0 ? "peak find" : "threshold") << std::endl
                    << "\tdetection threshold: " << _sigma_cutoff << std::endl
                    << "\tnumber of time samples: " << _current_time_samples << std::endl
                    << "\tmaximum number of candidates: " << _max_candidates << std::endl
                    << "\ttime averaging boxcar limits: ";
                    for (auto ilimit : _decimate_bc_limits) {
                        std::cout << ilimit << " ";
                    }
                    std::cout << std::endl
                    << "\taveraging iterations: " << _iterations << std::endl
                    << "\tmax requested boxcar width: " << _max_boxcar_width_desired << std::endl
                    << "\tmax performed boxcar width: " << _max_boxcar_width_performed << std::endl
                    << "\tused boxcar widths: ";
                    for (auto iwidth : _boxcar_widths) {
                        std::cout << iwidth << " ";
                    }
                    std::cout << std::endl << std::endl;
    }

} // namespace astroaccelerate
