#include "aa_sps_plan.hpp"

namespace astroaccelerate {

    aa_sps_plan::aa_sps_plan(void)
        : _candidate_algorithm(1)
        , _max_boxcar_width_in_sec(0.150f)
        , _sigma_cutoff(6.0f)
        , _sigma_constant(6.0f)
        // NOTE: This vector tells after how many boxcars to average the time series
        , _decimate_bc_limits({32, 16, 16, 16, 8}) 
    {
        
    }

    aa_sps_plan::aa_sps_plan(DedispersionStrategy const& dedispersion_strategy)
                    : _candidate_algorithm(1)
                    , _max_boxcar_width_in_sec(dedispersion_strategy.get_max_boxcar_width_in_sec())
                    , _sigma_cutoff(dedispersion_strategy.get_sigma_cutoff())
                    , _sigma_constant(dedispersion_strategy.get_sigma_constant())
                    , _decimate_bc_limits{32, 16, 16, 16, 8} 
    {

    }

    aa_sps_plan::aa_sps_plan(int candidate_algorithm, float max_boxcar_width_in_sec, float sigma_cutoff, float sigma_constant)
                    : _candidate_algorithm(candidate_algorithm)
                    , _max_boxcar_width_in_sec(max_boxcar_width_in_sec)
                    , _sigma_cutoff(sigma_cutoff)
                    , _sigma_constant(sigma_constant)
                    , _decimate_bc_limits{32, 16, 16, 16, 8}
    {

    }

} // namespaceastroaccelerate
